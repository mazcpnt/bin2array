# bin2array

文件二进制转化为C语言数组，方便在没有文件系统的MCU上操作文件。

对于一些不支持SD卡和Flash存储介质的设备，有的时候可能会需要读取二进制数据做处理。使用 bin2array 工具可以将文件转化为c数组，将文件二进制固化在ROM中供设备使用。

## 编译

```
$ make
gcc -I./ -c bin2array.c -o bin2array.o 
gcc bin2array.o -lpthread -o bin2array
```

## 用法

参数不合法将输出打印信息：

```
$ ./bin2array
usage: ./bin2array <file> <array.txt>
   eg: ./bin2array ./resource/colorbar.jpg array.txt
```

参数合法将进行转化：

```
$ ./bin2array ./resource/colorbar.jpg array.txt
input file length = 46928
output file length = 281568
```

## 验证

使用 HxD 查看 colorbar.jpg 文件的二进制数据，和 array.txt 数组内容做比对。

![verify](./document/readme-verify.png)



