#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* 输入输出文件结构体 */
typedef struct _MAZFILE_
{
    char name[32];              // 文件名
    FILE *fp;                   // 文件句柄
    unsigned char *buffer;      // 文件暂存数据的缓冲区
    unsigned int length;        // 文件大小
} MAZFILE;

int main (int argc, char *argv[])
{
    int i = 0;
    int len = 0;
    MAZFILE input;              // 输入图片文件
    MAZFILE output;             // 输出文本文件

    /* 判断参数格式是否正确 */
    if (argc != 3)
    {
        printf("usage: ./bin2array <file> <array.txt>\r\n");
        printf("   eg: ./bin2array ./resource/colorbar.jpg array.txt\r\n");
        return -1;
    }

    /* 记录输入输出文件名 */
    strcpy(input.name, argv[1]);
    strcpy(output.name, argv[2]);

    /* 打开输入文件 */
    input.fp = fopen(input.name, "rb");
    if (!input.fp)
    {
        printf("err: open %s file failed!\r\n", input.name);
        return -1;
    }

    /* 偏移到文件最后, 查询当前偏移值则为文件大小 */
    fseek(input.fp, 0L, SEEK_END);
    input.length = ftell(input.fp);
    printf("input file length = %d\r\n", input.length);

    /* 偏移回文件头 */
    fseek(input.fp, 0L, SEEK_SET);

    /* 读取输入文件数据 */
    input.buffer = malloc(input.length);
    if(NULL == input.buffer)
    {
        printf("err: malloc input buffer failed!\r\n");
        return -1;
    }

    len = fread(input.buffer, 1, input.length, input.fp);
    if(len != input.length)
    {
        printf("err: input read failed! cnt = %d, length = %d\r\n", (int)len, input.length);
        free(input.buffer);
        fclose(input.fp);
        return -1;
    }

    /* 输入文件的一个字节, 输出文件需要用 6 个字节表示 */
    /* 示例: 输入十六进制数 AB(h), 输出为字符串 "0xAB, " */
    output.length = input.length * 6;
    printf("output file length = %d\r\n", output.length);

    /* 将输出文件的二进制流转化为C语言数组写入输出文件中 */
    output.buffer = malloc(output.length);
    if(NULL == output.buffer)
    {
        printf("err: malloc output buffer failed!\r\n");
        return -1;
    }

    for(i = 0; i < input.length; i++)
    {
        /* 输出文件每 16 个字节输出一次换行 */
        if((i + 1) % 16 == 0)
        {
            sprintf(&output.buffer[6 * i], "0x%02x,\n", input.buffer[i]);
        }
        else
        {
            sprintf(&output.buffer[6 * i], "0x%02x, ", input.buffer[i]);
        }
    }

    /* 创建输出文件 */
    output.fp = fopen(output.name, "wb");
    if (!output.fp)
    {
        printf("err: open %s file failed!\r\n", output.name);
        return -1;
    }

    len = fwrite(output.buffer, 1, output.length, output.fp);
    if(len != output.length)
    {
        printf("err: output write failed! len = %d, length = %d\r\n", (int)len, output.length);
        free(input.buffer);
        free(output.buffer);
        fclose(input.fp);
        fclose(output.fp);
        return -1;
    }

    /* 释放帧缓冲区 */
    free(input.buffer);
    free(output.buffer);

    /* 关闭文件 */
    fclose(input.fp);
    fclose(output.fp);
    return 0;
}
