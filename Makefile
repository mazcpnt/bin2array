CROSS_COMPILE := 

CC      := $(CROSS_COMPILE)gcc
CXX     := $(CROSS_COMPILE)g++
LD      := $(CROSS_COMPILE)ld
AR      := $(CROSS_COMPILE)ar
RANLIB  := $(CROSS_COMPILE)ranlib
STRIP   := $(CROSS_COMPILE)strip

BIN     = bin2array
SOURCES = $(wildcard *.c)
HEADERS = $(wildcard *.h)
OBJECTS = $(patsubst %.c, %.o, $(SOURCES))

IFLAGS  = -I./
CFLAGS  = $(IFLAGS)
LFLAGS  = -lpthread

$(BIN) : $(OBJECTS)
	$(CC) $^ $(LFLAGS) -o $@

%.o:%.c
	$(CC) $(CFLAGS) -c $^ -o $@ 

clean :
	rm -f *.o  $(BIN)
